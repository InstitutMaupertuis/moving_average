[![Institut Maupertuis logo](http://www.institutmaupertuis.fr/media/gabarit/logo.png)](http://www.institutmaupertuis.fr)

This package contains a node that computes moving average values of ROS topics and publishes the results.

The values are keep for a certain amount of time (`window_length` param) so the number of values used to compute the moving average varies depending on the publishing rate of the topic. (if you are looking for a queue with `x` elements, this package is NOT what you are looking for)

# Running
Example:
```bash
rosrun moving_average republish _in_topic:=/node/topic_0 _out_topic:=/node/topic_0/mean _window_length:=5
```

# Supported topics
- `std_msgs/Float32.h`
- `std_msgs/Float64.h`
- `std_msgs/Int16.h`
- `std_msgs/Int32.h`
- `std_msgs/Int64.h`
- `std_msgs/Int8.h`
- `std_msgs/UInt16.h`
- `std_msgs/UInt32.h`
- `std_msgs/UInt64.h`
- `std_msgs/UInt8.h`
